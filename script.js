gsap.registerPlugin(ScrollTrigger);
const triggers = gsap.utils.toArray(".section_anim");

triggers.forEach((elem) => {
  gsap.to(elem, {
    scrollTrigger: {
      trigger: elem,
      markers: true,
      scrub: 0,
      start: "20% top",
      end: "bottom top",
    },
    scale: 0.9,
    ease: 'linear'
  });
});
